from django.db import models
from datetime import datetime

# Create your models here.


class Category(models.Model):
    title = models.CharField(max_length=300)
    primaryCategory = models.BooleanField(default=False)

    def __str__(self):
        return self.title


class Product(models.Model):
    mainImage = models.ImageField(upload_to='images/',blank=True)
    name = models.TextField(max_length=300)
    slug = models.SlugField()
    category = models.ForeignKey(Category,on_delete=models.CASCADE)
    preview_text = models.CharField(max_length=300)
    description_text = models.CharField(max_length=300)
    price = models.FloatField()
    timeStamp = models.DateTimeField(default=datetime.now,blank=True)

    def __str__(self):
        return self.name
